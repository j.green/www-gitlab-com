(function() {
  var tiles = document.getElementsByClassName('event-tile');
  var years = document.getElementsByClassName('year');
  var callouts = document.getElementsByClassName('callout');

  function showCallout() {
    if (this.classList.contains('expand-container-closed')) {
      // currently closed, remove the class and open the callout
      this.classList.remove('expand-container-closed');
      this.parentNode.classList.remove('callout-open');
    } else {
      // currently open, close the callout
      this.classList.add('expand-container-closed');
      this.parentNode.classList.add('callout-open');
    }
  }

  function calloutClickEvent() {
    for (var i = 0; i < callouts.length; i++) {
      if (callouts[i].offsetHeight >= 200) {
        var expandContainer = document.createElement('DIV');
        expandContainer.classList.add('expand-container');
        var expandIcon = document.createElement('i');
        expandIcon.classList.add('fas');
        expandIcon.classList.add('fa-caret-down');
        expandContainer.appendChild(expandIcon);
        expandContainer.addEventListener('click', showCallout);
        callouts[i].appendChild(expandContainer);
      }
    }
  }

  calloutClickEvent();

  function animateContent() {
    for (var i = 0; i < tiles.length; i++) {
      if (tiles[i].getBoundingClientRect().top <= (window.innerHeight / 2) + 100) {
        tiles[i].classList.add('event-tile-show');
      } else {
        tiles[i].classList.remove('event-tile-show');
      }
    }
  }

  function animateYears() {
    for (var i = 0; i < years.length; i++) {
      if (years[i].getBoundingClientRect().top === 80) {
        years[i].firstElementChild.classList.add('dot-active');
      } else {
        years[i].firstElementChild.classList.remove('dot-active');
      }
    }
  }

  function scrollEvent() {
    animateContent();
    animateYears();
  }

  window.addEventListener('scroll', scrollEvent);
})();
