---
layout: markdown_page
title: "RM.1.03 - Self-Assessments"
---
 
## On this page
{:.no_toc}
 
- TOC
{:toc}
 
# RM.1.03 - Self-Assessments
 
## Control Statement

Quarterly, reviews shall be performed with approved documented specification to confirm personnel are following security policies and operational procedures pertaining to:
* log reviews quarterly
* firewall rule-set reviews
* applying configuration standards to new systems
* responding to security alerts
* change management processes
 
## Context

Reviews ensure that the process we've designed are operating as intended. The above list represents the common ways attackers can exploit systems to steal, delete, or alter information.
 
## Scope
This control applies to:
   * GitLab.com
   * System that support the operation of GitLab.com
   * All applications that store or process financial data

## Ownership
TBD
 
## Guidance
TBD
 
## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Self-Assessments issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/868) . 
 
### Policy Reference
TBD
 
## Framework Mapping
TBD